<?php

require_once __DIR__ . '/../../vendor/autoload.php';

use Zend\Diactoros,
    Krak\Mw,
    Krak\HttpMessage\Match;

function onA404($rf) {
    return mw\mount('/a', function() use ($rf) {
        return $rf(404, [], 'Not Found');
    });
}

function onCAttr() {
    return mw\on('/c', function($req, $next) {
        return $next($req->withAttribute('x-attr', 'value'));
    });
}

function printReqAttributes() {
    return function($req, $next) {
        $resp = $next($req);
        if ($resp->getStatusCode() == 200) {
            $body = $resp->getBody();
            $body->write("<pre>" . print_r($req->getAttributes(), true) . "</pre>");
        }

        return $resp;
    };
}

function throwEx() {
    return function() {
        throw new Exception('yo! this is an exception message!');
    };
}

function exceptionHandler($rf) {
    return function($req, $e) use ($rf) {
        return $rf(500, [], 'An exception occurred - ' . $e->getMessage());
    };
}

$app = new Silex\Application();

$app->get('/a', function() {
    return 'a';
});
$app->get('/b', function() {
    return 'b';
});
$app->get('/c', function() {
    return 'c';
});
$app->finish(function($req, $resp) {
    exit('1212');
});

list($mw, $mwapp) = Krak\Mw\Symfony\symfonyFactory($app);
$rf = Krak\Mw\diactorosResponseFactory();
$rf = Krak\Mw\textResponseFactory($rf);

$kernel = Krak\Mw\mwHttpKernel([
    Krak\Mw\catchException(exceptionHandler($rf)),
    onA404($rf),
    onCAttr(),
    Krak\Mw\on('/b', throwEx()),
    printReqAttributes(),
    $mw,
]);

$mwapp($kernel);
